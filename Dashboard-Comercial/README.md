# Conferência Corp - Dashboard Comercial

## Build Setup

```bash
# install dependencies
npm ci

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## Install editor plugins

- eslint
- prettier
- editorconfig
- stylelint

# Atualização dos dados do dash

- Abrir o arquivo Json data/leads.json

- Inserir os valores abaixo:

- Valor da Meta
- Total de Leads
- Leads Qualificados
- Data de Inicio
- Semana.
- Convertidos em cada usuario dentro de Sellers campo converted
